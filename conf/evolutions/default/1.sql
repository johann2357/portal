# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table accounts_adminaccount (
  id                        bigint not null,
  nickname                  varchar(30) not null,
  email                     varchar(50) not null,
  password                  varchar(50) not null,
  real_name                 varchar(255),
  constraint uq_accounts_adminaccount_nicknam unique (nickname),
  constraint uq_accounts_adminaccount_email unique (email),
  constraint pk_accounts_adminaccount primary key (id))
;

create table media_category (
  id                        bigint not null,
  name                      varchar(255) not null,
  parent_id                 bigint,
  description               varchar(255),
  constraint pk_media_category primary key (id))
;

create table media_file (
  id                        bigint not null,
  name                      varchar(255) not null,
  size                      bigint not null,
  author                    varchar(255),
  description               varchar(255),
  path                      varchar(255) not null,
  price                     float not null,
  rate                      integer,
  state                     varchar(2) not null,
  category_id               bigint,
  constraint uq_media_file_path unique (path),
  constraint pk_media_file primary key (id))
;

create table purchase_gift (
  id                        bigint not null,
  purchased_file_id         bigint not null,
  from_user_id              bigint not null,
  to_user_id                bigint not null,
  date                      timestamp not null,
  constraint pk_purchase_gift primary key (id))
;

create table accounts_moneyaccount (
  id                        bigint not null,
  amount                    float not null,
  related_user_id           bigint not null,
  constraint pk_accounts_moneyaccount primary key (id))
;

create table media_permanentpromotion (
  id                        bigint not null,
  name                      varchar(255) not null,
  discount                  float not null,
  begin_date                timestamp not null,
  description               varchar(255),
  category_id               bigint not null,
  money_required            float not null,
  constraint uq_media_permanentpromotion_name unique (name),
  constraint pk_media_permanentpromotion primary key (id))
;

create table purchase_purchase (
  id                        bigint not null,
  related_user_id           bigint not null,
  related_file_id           bigint not null,
  total_spend               float not null,
  state                     varchar(2) not null,
  date                      timestamp not null,
  rate                      integer,
  constraint pk_purchase_purchase primary key (id))
;

create table media_temporarypromotion (
  id                        bigint not null,
  name                      varchar(255) not null,
  discount                  float not null,
  begin_date                timestamp not null,
  description               varchar(255),
  category_id               bigint not null,
  end_date                  timestamp not null,
  constraint uq_media_temporarypromotion_name unique (name),
  constraint pk_media_temporarypromotion primary key (id))
;

create table accounts_useraccount (
  id                        bigint not null,
  nickname                  varchar(30) not null,
  email                     varchar(50) not null,
  password                  varchar(50) not null,
  real_name                 varchar(255),
  state                     varchar(2) not null,
  activation_date           timestamp not null,
  constraint uq_accounts_useraccount_nickname unique (nickname),
  constraint uq_accounts_useraccount_email unique (email),
  constraint pk_accounts_useraccount primary key (id))
;

create sequence accounts_adminaccount_seq;

create sequence media_category_seq;

create sequence media_file_seq;

create sequence purchase_gift_seq;

create sequence accounts_moneyaccount_seq;

create sequence media_permanentpromotion_seq;

create sequence purchase_purchase_seq;

create sequence media_temporarypromotion_seq;

create sequence accounts_useraccount_seq;

alter table media_category add constraint fk_media_category_parent_1 foreign key (parent_id) references media_category (id);
create index ix_media_category_parent_1 on media_category (parent_id);
alter table media_file add constraint fk_media_file_category_2 foreign key (category_id) references media_category (id);
create index ix_media_file_category_2 on media_file (category_id);
alter table purchase_gift add constraint fk_purchase_gift_purchased_fil_3 foreign key (purchased_file_id) references purchase_purchase (id);
create index ix_purchase_gift_purchased_fil_3 on purchase_gift (purchased_file_id);
alter table purchase_gift add constraint fk_purchase_gift_from_user_4 foreign key (from_user_id) references accounts_useraccount (id);
create index ix_purchase_gift_from_user_4 on purchase_gift (from_user_id);
alter table purchase_gift add constraint fk_purchase_gift_to_user_5 foreign key (to_user_id) references accounts_useraccount (id);
create index ix_purchase_gift_to_user_5 on purchase_gift (to_user_id);
alter table accounts_moneyaccount add constraint fk_accounts_moneyaccount_relat_6 foreign key (related_user_id) references accounts_useraccount (id);
create index ix_accounts_moneyaccount_relat_6 on accounts_moneyaccount (related_user_id);
alter table media_permanentpromotion add constraint fk_media_permanentpromotion_ca_7 foreign key (category_id) references media_category (id);
create index ix_media_permanentpromotion_ca_7 on media_permanentpromotion (category_id);
alter table purchase_purchase add constraint fk_purchase_purchase_related_u_8 foreign key (related_user_id) references accounts_useraccount (id);
create index ix_purchase_purchase_related_u_8 on purchase_purchase (related_user_id);
alter table purchase_purchase add constraint fk_purchase_purchase_related_f_9 foreign key (related_file_id) references media_file (id);
create index ix_purchase_purchase_related_f_9 on purchase_purchase (related_file_id);
alter table media_temporarypromotion add constraint fk_media_temporarypromotion_c_10 foreign key (category_id) references media_category (id);
create index ix_media_temporarypromotion_c_10 on media_temporarypromotion (category_id);



# --- !Downs

drop table if exists accounts_adminaccount cascade;

drop table if exists media_category cascade;

drop table if exists media_file cascade;

drop table if exists purchase_gift cascade;

drop table if exists accounts_moneyaccount cascade;

drop table if exists media_permanentpromotion cascade;

drop table if exists purchase_purchase cascade;

drop table if exists media_temporarypromotion cascade;

drop table if exists accounts_useraccount cascade;

drop sequence if exists accounts_adminaccount_seq;

drop sequence if exists media_category_seq;

drop sequence if exists media_file_seq;

drop sequence if exists purchase_gift_seq;

drop sequence if exists accounts_moneyaccount_seq;

drop sequence if exists media_permanentpromotion_seq;

drop sequence if exists purchase_purchase_seq;

drop sequence if exists media_temporarypromotion_seq;

drop sequence if exists accounts_useraccount_seq;

