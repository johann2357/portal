BEGIN;
CREATE TABLE "purchase_purchase" (
    "id" serial NOT NULL PRIMARY KEY,
    "related_user_id" integer NOT NULL REFERENCES "accounts_useraccount" ("id") DEFERRABLE INITIALLY DEFERRED,
    "related_file_id" integer NOT NULL REFERENCES "media_file" ("id") DEFERRABLE INITIALLY DEFERRED,
    "total_spend" double precision NOT NULL,
    "state" varchar(2) NOT NULL,
    "date" timestamp with time zone NOT NULL,
    "rate" integer NOT NULL
)
;
CREATE TABLE "purchase_gift" (
    "id" serial NOT NULL PRIMARY KEY,
    "purchased_file_id" integer NOT NULL UNIQUE REFERENCES "purchase_purchase" ("id") DEFERRABLE INITIALLY DEFERRED,
    "from_user_id" integer NOT NULL REFERENCES "accounts_useraccount" ("id") DEFERRABLE INITIALLY DEFERRED,
    "to_user_id" integer NOT NULL REFERENCES "accounts_useraccount" ("id") DEFERRABLE INITIALLY DEFERRED,
    "date" timestamp with time zone NOT NULL
)
;
CREATE INDEX "purchase_purchase_related_user_id" ON "purchase_purchase" ("related_user_id");
CREATE INDEX "purchase_purchase_related_file_id" ON "purchase_purchase" ("related_file_id");
CREATE INDEX "purchase_gift_from_user_id" ON "purchase_gift" ("from_user_id");
CREATE INDEX "purchase_gift_to_user_id" ON "purchase_gift" ("to_user_id");

COMMIT;
