BEGIN;
CREATE TABLE "media_category" (
    "id" serial NOT NULL PRIMARY KEY,
    "name" varchar(250) NOT NULL,
    "description" text NOT NULL,
    "parent_id" integer REFERENCES "media_category" ("id") DEFERRABLE INITIALLY DEFERRED
)
;
CREATE TABLE "media_temporarypromotion" (
    "id" serial NOT NULL PRIMARY KEY,
    "name" varchar(250) NOT NULL,
    "discount" double precision NOT NULL,
    "begin_date" date NOT NULL,
    "description" text NOT NULL,
    "category_id" integer NOT NULL REFERENCES "media_category" ("id") DEFERRABLE INITIALLY DEFERRED,
    "end_date" date NOT NULL
)
;
CREATE TABLE "media_permanentpromotion" (
    "id" serial NOT NULL PRIMARY KEY,
    "name" varchar(250) NOT NULL,
    "discount" double precision NOT NULL,
    "begin_date" date NOT NULL,
    "description" text NOT NULL,
    "category_id" integer NOT NULL REFERENCES "media_category" ("id") DEFERRABLE INITIALLY DEFERRED,
    "money_required" double precision NOT NULL
)
;
CREATE TABLE "media_file" (
    "id" serial NOT NULL PRIMARY KEY,
    "name" varchar(250) NOT NULL,
    "size" integer NOT NULL,
    "author" varchar(100) NOT NULL,
    "path" varchar(250) NOT NULL,
    "description" text NOT NULL,
    "price" double precision NOT NULL,
    "rate" double precision NOT NULL,
    "state" varchar(2) NOT NULL,
    "category_id" integer REFERENCES "media_category" ("id") DEFERRABLE INITIALLY DEFERRED
)
;
CREATE INDEX "media_category_parent_id" ON "media_category" ("parent_id");
CREATE INDEX "media_temporarypromotion_category_id" ON "media_temporarypromotion" ("category_id");
CREATE INDEX "media_permanentpromotion_category_id" ON "media_permanentpromotion" ("category_id");
CREATE INDEX "media_file_category_id" ON "media_file" ("category_id");

COMMIT;
