BEGIN;
CREATE TABLE "accounts_moneyaccount" (
    "id" serial NOT NULL PRIMARY KEY,
    "amount" double precision NOT NULL,
    "related_user_id" integer NOT NULL UNIQUE
)
;
CREATE TABLE "accounts_adminaccount" (
    "id" serial NOT NULL PRIMARY KEY,
    "nickname" varchar(30) NOT NULL UNIQUE,
    "email" varchar(30) NOT NULL UNIQUE,
    "real_name" varchar(100) NOT NULL,
    "password" varchar(30) NOT NULL
)
;
CREATE TABLE "accounts_useraccount" (
    "id" serial NOT NULL PRIMARY KEY,
    "nickname" varchar(30) NOT NULL UNIQUE,
    "email" varchar(30) NOT NULL UNIQUE,
    "real_name" varchar(100) NOT NULL,
    "password" varchar(30) NOT NULL,
    "state" boolean NOT NULL,
    "activation_date" date NOT NULL
)
;
ALTER TABLE "accounts_moneyaccount" ADD CONSTRAINT "related_user_id_refs_id_744ff57f" FOREIGN KEY ("related_user_id") REFERENCES "accounts_useraccount" ("id") DEFERRABLE INITIALLY DEFERRED;
CREATE INDEX "accounts_adminaccount_nickname_like" ON "accounts_adminaccount" ("nickname" varchar_pattern_ops);
CREATE INDEX "accounts_adminaccount_email_like" ON "accounts_adminaccount" ("email" varchar_pattern_ops);
CREATE INDEX "accounts_useraccount_nickname_like" ON "accounts_useraccount" ("nickname" varchar_pattern_ops);
CREATE INDEX "accounts_useraccount_email_like" ON "accounts_useraccount" ("email" varchar_pattern_ops);

COMMIT;
