package models;

import play.db.ebean.Model;
import javax.persistence.*;
import java.util.List;


@Entity
@Table(name="accounts_adminaccount")
public class AdminAccount extends Account {
  
    // ESTE FINDER TIENE QUE ESTAR EN TODAS LAS CLASES
    // http://www.playframework.com/documentation/2.2.3/api/java/play/db/ebean/Model.Finder.html
    public static Finder<Long, AdminAccount> find
        = new Model.Finder<>(Long.class, AdminAccount.class);

    public void add(String nickname, String email, String password, String real_name) {
        this.nickname = nickname;
        this.email = email;
        this.password = password;
        this.real_name = real_name;
        this.save();
    }

    public List<AdminAccount> get_all() {
        return this.find.all();
    }

    public AdminAccount get_by_email(String email) {
        return this.find.where().eq("email", email).findUnique();
    }

    public List<AdminAccount> get_by_real_name(String real) {
        return this.find.where().eq("real_name", real).findList();
    }
}
