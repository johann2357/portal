package models;

import play.db.ebean.Model;
import javax.persistence.*;


@Entity
@Table(name="accounts_moneyaccount")
public class MoneyAccount extends Model {

    @Id
    public Long id;

    @Column(nullable=false)
    public float amount; 

    @OneToOne(optional=false)
    public UserAccount related_user;

    // ESTE FINDER TIENE QUE ESTAR EN TODAS LAS CLASES
    public static Finder<Long, MoneyAccount> find
        = new Model.Finder<>(Long.class, MoneyAccount.class);

    public static void decrease (UserAccount usr, float money){
        MoneyAccount ac = MoneyAccount.find.where().eq("related_user", usr).findUnique();
    	ac.amount = ac.amount - money;
        ac.update();
    }

    public static float get_current_money ( UserAccount usr ){
    	return MoneyAccount.find.where().eq("related_user", usr).findUnique().amount;
    }

    public void increase ( float money) {
        this.amount = (this.amount + money );
        this.update();
    }

}
