package models;

import play.db.ebean.Model;
import java.util.Date;
import javax.persistence.*;


@Entity
@Table(name="accounts_useraccount")
public class UserAccount extends Account {

    @Column(length=2, nullable=false)
    public String state;

    @Column(nullable=false)
    public Date activation_date;

    public static Finder<Long, UserAccount> find
        = new Model.Finder<>(Long.class, UserAccount.class);

    public void add(String nickname, String email, String password,
            String real_name, String state, Date activation_date) {
        this.nickname = nickname;
        this.email = email;
        this.password = password;
        this.real_name = real_name;
        this.state = state;
        this.activation_date = activation_date;
        this.save();
        MoneyAccount user_money_account = new MoneyAccount();
        user_money_account.amount = (float)0;
        user_money_account.related_user = this;
        user_money_account.save();
    }

    public void activate(Long id)
    {
        UserAccount x = UserAccount.find.byId(id);
        x.state="ok";
        x.save();
    }

    
    public void deactivate(Long id)
    {
        UserAccount x = UserAccount.find.byId(id);
        x.state="NA";
        x.save();     
    }

    public void get_purchase_history()
    {


    }

    public void get_gifts_history()
    {

    }

    public static void add(UserAccount userA){
        Date now = new Date();
        userA.state = "OK";
        userA.activation_date = now;
        userA.save();
        MoneyAccount user_money_account = new MoneyAccount();
        user_money_account.amount = (float)0;
        user_money_account.related_user = userA;
        user_money_account.save();
    }

    public static void getAccount(UserAccount userAcc){
        UserAccount tmp = UserAccount.find.byId(userAcc.id);
        userAcc.nickname = tmp.nickname;
        userAcc.email = tmp.email;
        userAcc.real_name = tmp.real_name;

        System.out.println(userAcc.nickname + "algo");
        System.out.println(userAcc.email+ "algo");
        System.out.println(userAcc.real_name+ "algo");
    }

}