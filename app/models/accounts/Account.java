package models;

import play.db.ebean.Model;
import javax.persistence.*;


@MappedSuperclass
public abstract class Account extends Model {

    @Id
    public Long id;

    @Column(length=30, unique=true, nullable=false)
    public String nickname;

    @Column(length=50, unique=true, nullable=false)
    public String email;

    @Column(length=50, nullable=false)
    public String password;

    public String real_name;
}
