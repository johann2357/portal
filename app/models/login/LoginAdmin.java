package models;

import models.UserAccount;
import play.db.ebean.Model;
import javax.persistence.*;

public class LoginAdmin extends Model {
    public static Finder<String,AdminAccount> findadmin = new Finder(String.class,AdminAccount.class);

	public String email;
    public String password;

    public String validate() {
	    if (authenticate(email, password) == null) {
	      return "Invalid email or password";
	    }
	    return null;
	}

	public String authenticate(String _email, String _pass){
        AdminAccount userAcc = findadmin.where().eq("email",_email).findUnique();
        String cad = null;
        if (userAcc == null) {
            return null;
        }else{
            String tmp = userAcc.password;
            if (tmp.equals(_pass)) {
                cad = _pass;
            }
        }
        return cad;
	}

}
