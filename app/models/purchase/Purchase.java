package models;

import play.db.ebean.Model;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


@Entity
@Table(name="purchase_purchase")
public class Purchase extends Model {

    @Id
    public Long id;

    @ManyToOne(optional=false)
    public UserAccount related_user;

    @ManyToOne(optional=false)
    public File related_file;

    @Column(nullable=false)
    public float total_spend;

    @Column(length=2, nullable=false)
    public String state;

    @Column(nullable=false)
    public Date date;

    public int rate;

    public static Finder<Long, Purchase> find
        = new Model.Finder<>(Long.class, Purchase.class);

    public void add(UserAccount related_user, File related_file,
            float total_spend, String state, Date date, int rate) {
        this.related_user = related_user;
        this.related_file = related_file;
        this.total_spend = total_spend;
        this.state = state;
        this.date = date;
        this.rate = rate;
        this.save();
    }

    public void set_rate(int rating) {
        this.rate = rating;
        this.update();
    }

    public List<Purchase> get_all(String state) {
        return this.find.all();
    }

    public List<Purchase> get_by_user(UserAccount user) {
        return this.find.where().eq("related_user", user).findList();
    }

    public List<Purchase> get_by_file(File file) {
        return this.find.where().eq("related_file", file).findList();
    }

    public List<Purchase> get_by_state(String state) {
        return this.find.where().eq("state", state).findList();
    }

    public static float get_total_spend(UserAccount usr) {
        List<Purchase> ps = Purchase.find.where().eq("related_user", usr).findList();
        float result = 0;
        for (Purchase p : ps) {
            result += p.total_spend;
        }
        return result;
    }
}
