package models;

import play.db.ebean.Model;
import javax.persistence.*;
import java.util.Date;



@Entity
@Table(name="purchase_gift")
public class Gift extends Model {

public static Finder<Long, Gift> find
        = new Model.Finder<>(Long.class, Gift.class);
    @Id
    public Long id;

    @OneToOne(optional=false)
    public Purchase purchased_file;

    @ManyToOne(optional=false)
    public UserAccount from_user;

    @ManyToOne(optional=false)
    public UserAccount to_user;
    
    @Column(nullable=false)
    public Date date;

    public void add(Purchase purchased, UserAccount to_user) {
       
        date = new Date();
        this.purchased_file = purchased;
        this.to_user = to_user;
        this.save();
    }

    

}
