package models;

import play.db.ebean.Model;
import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.text.SimpleDateFormat;
import java.text.ParseException;

import java.util.*;
import play.data.format.*;
import play.data.validation.Constraints.*;
import play.data.validation.*;
import com.avaje.ebean.*;

@Entity
@Table(name="media_permanentpromotion")
public class PermanentPromotion extends Promotion {


	public static Finder<Long,PermanentPromotion> find = new Finder(Long.class, PermanentPromotion.class);

	@Column(nullable=false)
	public float money_required;

	public static synchronized java.util.Date StringToDate(String date) {
		SimpleDateFormat text_Format = new SimpleDateFormat("dd-MM-yyyy");
		Date init_date = null;
		try {
			init_date = text_Format.parse(date);
			return init_date;
		} catch (ParseException ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public static String getFecha(Date fecha) {
        SimpleDateFormat formateador = new SimpleDateFormat("dd-MM-yyyy");
        return formateador.format(fecha);
    }


	public boolean single_check(File file){
		if(file.price > this.money_required){
			return true;
		}
		return false;
	}

	public boolean multiple_check(List<File> list_file){
		float res = 0;
		for (File f : list_file) {
			res += f.price;
		}

		if(res > this.money_required){
			return true;
		}
		return false;	
	}

	/*
	public void add(String name, float discount, String begin_date, String description, Category category, float money_required) {
		this.name 		= name;
		this.discount 	= discount;
		//Date d = StringToDate(begin_date);
		this.begin_date = StringToDate(begin_date);
		this.description = description;
		this.category 	= category;
		this.money_required = money_required;
		this.save();
	}
	*/

	public static List<PermanentPromotion> all_PermanentPromotion() {
		return find.all();
	}

	public List<PermanentPromotion> get_all() {
		return this.find.all();
	}

	public PermanentPromotion get_by_name(String name) {
		return this.find.where().eq("name", name).findUnique();
	}

	public List<PermanentPromotion> get_by_discount(float discount) {
		return this.find.where().eq("discount", discount).findList();
	}

	public List<PermanentPromotion> get_by_beginDate(String date) {
		return this.find.where().eq("begin_date", StringToDate(date)).findList();
	}

	public List<PermanentPromotion> get_by_category(Category category) {
		return this.find.where().eq("category", category).orderBy("begin_date desc").findList();
	}

	public static float get_best_promotion(File ff, float actual_money) {
		List<PermanentPromotion> pps = PermanentPromotion.find.where().eq(
			"category", ff.category
		).orderBy("discount desc").findList();
		if ((pps == null) && (pps.isEmpty())) {
			return 0;
		}
		for (PermanentPromotion p : pps) {
			if (p.money_required <= actual_money) {
				return p.discount;
			}
		}
		return 0;
	}

}
