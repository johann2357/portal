package models;

import play.db.ebean.Model;
import javax.persistence.*;
import java.util.*;
import play.data.validation.Constraints.*;
import play.data.validation.ValidationError;

@Entity
@Table(name="media_file")
public class File extends Model {

    public static Finder<Long,File> find = new Finder(Long.class, File.class);

    @Id
    public Long id;

    @Column(nullable=false)
    public String name;

    @Column(nullable=false)
    public Long size;

    public String author;
    
    public String description;

    @Column(unique=true, nullable=false)
    public String path;

    @Column(nullable=false)
    public float price;

    public int rate;

    @Column(length=2, nullable=false)
    public String state;

    @ManyToOne
    public Category category;

    public List<ValidationError> validate() { 
        // null indica que esta bien ...  caso contrario retorna el string como mensaje
        List<ValidationError> errors = new ArrayList<ValidationError>(); 
        // validando price
        if (this.price <= 0) {
            errors.add(new ValidationError("price",  "Price can't be a negative number nor 0"));
        }
        // validando rate
        if ((this.rate < 0) || (this.rate > 10)) {
            errors.add(new ValidationError("rate",  "Rate must be between 0 and 10"));
        }
        // validando state
        if (!((this.state.equals("OK")) || (this.state.equals("NN")))) {
            errors.add(new ValidationError("state",  "Option is not valid. Valid options ('OK', 'NN')"));
        }
        return errors.isEmpty() ? null : errors;
    }

    public static List<File> all_File() {
        return find.all();
    }

    public  static void changeState(Long _id){
        File anyFile = File.find.byId(_id);
        anyFile.state = "2";
        anyFile.save();
    }

}
