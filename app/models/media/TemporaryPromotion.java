package models;

import play.db.ebean.Model;
import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import play.data.validation.ValidationError;

import java.util.*;
import play.data.format.*;
import play.data.validation.*;
import com.avaje.ebean.*;

@Entity
@Table(name="media_temporarypromotion")
public class TemporaryPromotion extends Promotion {

	public static Finder<Long, TemporaryPromotion> find
		= new Model.Finder<>(Long.class, TemporaryPromotion.class);

    @Column(nullable=false)
    public Date end_date;


    public static synchronized java.util.Date StringToDate(String date) {
		SimpleDateFormat text_Format = new SimpleDateFormat("dd-MM-yyyy");
		Date init_date = null;
		try {
			init_date = text_Format.parse(date);
			return init_date;
		} catch (ParseException ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public static String getFecha(Date fecha) {
        SimpleDateFormat formateador = new SimpleDateFormat("dd-MM-yyyy");
        return formateador.format(fecha);
    }

	public boolean is_valid(Category category){
		if(category.name == this.category.name && this.end_date.before(new Date())){//this.end_date == new Date()){
			return true;
		}
		return false;	
	}

	public void add(String name, float discount, String begin_date, String description, Category category, String end_date) {
		this.name 		= name;
		this.discount 	= discount;
		this.begin_date = StringToDate(begin_date);
		this.description = description;
		this.category 	= category;
		this.end_date 	= StringToDate(end_date);
		this.save();
	}

	public static List<TemporaryPromotion> get_all() {
		return find.all();
	}

	public TemporaryPromotion get_by_name(String name) {
		return this.find.where().eq("name", name).findUnique();
	}

	public List<TemporaryPromotion> get_by_discount(float discount) {
		return this.find.where().eq("discount", discount).findList();
	}

	public List<TemporaryPromotion> get_by_beginDate(String bdate) {
		return this.find.where().eq("begin_date", StringToDate(bdate)).findList();
	}

	public List<TemporaryPromotion> get_by_endDate(String edate) {
		return this.find.where().eq("end_date", StringToDate(edate)).findList();
	}

	public List<TemporaryPromotion> get_by_category(Category category) {
		return this.find.where().eq("category", category).findList();
	}

	public static float get_best_promotion(File ff) {
		List<TemporaryPromotion> tps = TemporaryPromotion.find.where().eq(
			"category", ff.category
		).orderBy("discount desc").findList();
		if ((tps == null) && (tps.isEmpty())) {
			return 0;
		}
		Date today = new Date();
		for (TemporaryPromotion p : tps) {
			if (p.end_date.before(today)) {
				return p.discount;
			}
		}
		return 0;
	}

}
