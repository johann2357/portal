package models;

import play.db.ebean.Model;
import javax.persistence.*;
import java.util.Date;


@MappedSuperclass
public abstract class Promotion extends Model {

	@Id
	public Long id;

	@Column(nullable=false, unique=true)
	public String name;

	@Column(nullable=false)
	public float discount;
	
	@Column(nullable=false)
	public Date begin_date;

	public String description;

	@ManyToOne(optional=false)
	public Category category;

}

