package models;

import play.db.ebean.Model;
import javax.persistence.*;
import java.util.*;
import play.data.validation.Constraints.*;
import play.data.validation.ValidationError;

@Entity
@Table(name="media_category")
public class Category extends Model {

    public static Finder<Long,Category> find = new Finder(Long.class, Category.class);
    
    @Id
    public Long id;

    @Column(nullable=false)
    public String name;
    
    @ManyToOne  
    public Category parent;

    public String description;

    // a traves de this.parent se puede acceder a las categorias
    //public List<Category> sub_categories;

    public void create(String name, Category parent, String description) 
	{
		this.name 		 	= name;
		this.parent 	 	= parent;
		this.description 	= description;
		this.save();
	}

    public List<Category> get_all() {
        return find.all();
    }
    
    public Category get_by_name(String name) {
        return this.find.where().eq("name", name).findUnique();
    }

    /* FIXME: necesario ... pero debe hacerse de otra forma
    public static List<Category> get_sub(String name) {
        return this.get_by_name(name).sub_categories;
    }
    */

    public Category get_parent(String name) {
        return this.get_by_name(name).parent;
    }

}
