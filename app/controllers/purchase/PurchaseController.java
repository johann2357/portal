package controllers;
import play.*;
import play.mvc.*;
import play.data.Form;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import views.html.*;
import models.*;
import controllers.LoginController;
import forms.PurchaseForm;

public class PurchaseController extends Controller {

    static Form<PurchaseForm> form = Form.form(PurchaseForm.class);

    public static Result user_purchase_list() {
        if (! LoginController.is_user()) {
            return redirect(routes.LoginController.login());
        }
        UserAccount usr = UserAccount.find.where().eq("email", session().get("email")).findUnique();
        return ok(views.html.purchase.list_purchases.render(
            Purchase.find.where().eq("related_user", usr).findList()
        ));
    }

    public static Result user_buy(Long id) {
        if (! LoginController.is_user()) {
            return redirect(routes.LoginController.login());
        }
        UserAccount usr = UserAccount.find.where().eq("email", session().get("email")).findUnique();
        File instance = File.find.byId(id);
        if (instance != null) {
            float actual_money = MoneyAccount.get_current_money(usr);
            if (instance.price > actual_money) {
                return ok("YOU DONT HAVE ENOUGH MONEY");
            }
            float total_spend = Purchase.get_total_spend(usr);
            Purchase prs = Purchase.find.where().eq(
                "related_user", usr).eq("related_file", instance).findUnique();
            if (prs != null) {
                return ok("YOU ALREADY HAS THAT FILE :D");
            }
            prs = new Purchase();
            prs.related_user = usr;
            prs.related_file = instance;
            float discount_pp = PermanentPromotion.get_best_promotion(instance, actual_money);
            float discount_tp = TemporaryPromotion.get_best_promotion(instance);
            float best_prom  = discount_pp;
            if (discount_tp > discount_pp) {
                best_prom = discount_tp;
            }
            prs.total_spend = instance.price - instance.price * best_prom;
            prs.rate = 0;
            prs.state = "FB";
            prs.date = new Date();
            prs.save();
            MoneyAccount.decrease(usr, prs.total_spend);
            return redirect(routes.PurchaseController.user_purchase_list());
        }
        return ok("NO SUCH FILE :(");
    }

    public static Result user_gift_list() {
        if (! LoginController.is_user()) {
            return redirect(routes.LoginController.login());
        }
        return TODO;
    }

    public static Result user_present_list() {
        if (! LoginController.is_user()) {
            return redirect(routes.LoginController.login());
        }
        return TODO;
    }

    public static Result user_download(Long id) {
        if (! LoginController.is_user()) {
            return redirect(routes.LoginController.login());
        }
        UserAccount usr = UserAccount.find.where().eq("email", session().get("email")).findUnique();
        File instance = File.find.byId(id);
        Purchase prs = Purchase.find.where().eq(
            "related_user", usr).eq("related_file", instance).findUnique();
        if (prs == null) {
            return ok("YOU DONT HAVE ACCESS TO THIS FILE :(");
        }
        prs.state = "DF";
        prs.update();
        return ok(new java.io.File(instance.path));
    }

    public static Result user_rate(Long id) {
        if (! LoginController.is_user()) {
            return redirect(routes.LoginController.login());
        }
        Purchase instance = Purchase.find.byId(id);
        if (instance != null) {
            PurchaseForm tmp = new PurchaseForm();
            tmp.fill_instance(instance);
            return ok(views.html.purchase.rate_purchase.render( form.fill(tmp) ));
        }
        return ok("NO SUCH PURCHASE");
    }

    public static Result user_rate_file() {
        if (! LoginController.is_user()) {
            return redirect(routes.LoginController.login());
        }
        Form<PurchaseForm> filledForm = form.bindFromRequest();
        if(filledForm.hasErrors()) {
            return badRequest(views.html.purchase.rate_purchase.render(filledForm));
        } else {
            PurchaseForm ff = filledForm.get();
            Purchase saved_ctg = Purchase.find.byId(ff.id);
            File instance = File.find.byId(ff.file);
            saved_ctg.rate = ff.rate;
            instance.rate = instance.rate + ff.rate;
            instance.save();
            saved_ctg.save();
            return redirect(routes.FileController.user_list());
        }
    }

}
