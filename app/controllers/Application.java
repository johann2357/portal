package controllers;

import play.*;
import play.mvc.*;
import java.util.Date;
import views.html.*;
import models.*;

public class Application extends Controller {

    public static Result index() {
        // AdminAccount ad = new AdminAccount();
        // VER ADMINACCOUNT PARA REFERENCIA
        // DOCUMENTACION DE FINDER
        // http://www.playframework.com/documentation/2.2.3/api/java/play/db/ebean/Model.Finder.html
        //ad.add("admin2", "admin2@admin.com", "admin1", "Administrator");
        //List<AdminAccount> result = ad.get_all();
        //List<AdminAccount> result = ad.get_by_real_name("Administrator");
        //for (AdminAccount r: result) {
        //    System.out.println(r.email);
        //}
        //System.out.println(ad.get_by_email("admin@admin.com").nickname);
        //System.out.println("HOLA");

        // UserAccount ua = new UserAccount();
        // ua.add("userx", "userx@portal.com", "userx", "User", "OK", new Date());

        //File file = new File();
        //file.name = "File3";
        //file.size = 1234;
        //file.path = "path/File3.txt";
        //file.price = (float)22.9;
        //file.state = "OK";
        //file.save();

        //Purchase p = new Purchase();
        //p.add(ua, file, 1234, "OK", new Date(), -1);
        //System.out.println("Holas");

        return ok(index.render("Bienvenidos al Portal de Descargas UCSP"));
    }

}
