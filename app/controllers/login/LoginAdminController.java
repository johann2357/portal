package controllers;

import play.*;
import play.mvc.*;
import views.html.*;
import models.*;
import play.data.*;


import static play.data.Form.*;

public class LoginAdminController extends Controller {

    public static Result login() {
        return ok( views.html.login.loginAdmin.render(Form.form(LoginAdmin.class)) );
    }

    public static Result authenticate() {
    	Form<LoginAdmin> loginForm = Form.form(LoginAdmin.class).bindFromRequest();

	    if (loginForm.hasErrors()) {
	        return badRequest( views.html.login.loginAdmin.render(loginForm) );
	    } else {
            session().clear();
            session("email", loginForm.get().email);
            session("type",  "admin");
            return redirect( routes.AdminController.index() );
        }
    } 

    public static Result err(){
        return ok(error.render("prueba","prueba2"));
    }

    public static Result logout(){
    	session().clear();
    	return redirect( routes.Application.index() );
    }
}
