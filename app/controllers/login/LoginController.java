package controllers;

import play.*;
import play.mvc.*;
import views.html.*;
import models.*;
import play.data.*;


import static play.data.Form.*;

public class LoginController extends Controller {

    public static Result login() {
        return ok( views.html.login.login.render(Form.form(Login.class)) );
    }

    public static Result authenticate() {
    	Form<Login> loginForm = Form.form(Login.class).bindFromRequest();

	    if (loginForm.hasErrors()) {
	        return badRequest( views.html.login.login.render(loginForm) );
	    } else {
            session().clear();
            session("email", loginForm.get().email);
            session("type",  "user");
            return redirect( routes.FileController.user_list() );
        }
    } 

    public static boolean is_user() {
        return ("user".equals(session().get("type")));
    }

    public static boolean is_admin() {
        return ("admin".equals(session().get("type")));
	}

    public static Result err(){
        return ok(error.render("prueba","prueba2"));
    }

    public static Result logout(){
    	session().clear();
    	return redirect( routes.Application.index() );
    }
}
