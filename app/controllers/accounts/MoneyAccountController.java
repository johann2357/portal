package controllers;
import play.*;
import play.mvc.*;
import play.data.Form;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import views.html.*;
import models.*;
import forms.MoneyAccountForm;

public class MoneyAccountController extends Controller {

    static Form<MoneyAccountForm> acForm = Form.form(MoneyAccountForm.class);

    public static Result list() {
        if (! LoginController.is_admin()) {
            return redirect(routes.LoginAdminController.login());
        }
        //UserAccount ua = new UserAccount();
        //ua.add("tester", "tester@portal.com", "tester", "Tester", "OK", new Date());
        return ok(views.html.admin.list_money_account.render( MoneyAccount.find.all() ));
    }

    public static Result update() {
        if (! LoginController.is_admin()) {
            return redirect(routes.LoginAdminController.login());
        }
        Form<MoneyAccountForm> filledForm = acForm.bindFromRequest();
        if(filledForm.hasErrors()) {
            return badRequest(views.html.admin.edit_money_account.render(filledForm));
        } else {
            MoneyAccountForm ff = filledForm.get();
            MoneyAccount saved_ac = MoneyAccount.find.byId(ff.id);
            saved_ac.id = ff.id;
            saved_ac.amount = ff.amount;
            saved_ac.update();
            return redirect(routes.MoneyAccountController.list());
        }
    }

    public static Result edit(Long id) {
        if (! LoginController.is_admin()) {
            return redirect(routes.LoginAdminController.login());
        }
        MoneyAccount instance = MoneyAccount.find.byId(id);
        if (instance != null) {
            MoneyAccountForm tmp = new MoneyAccountForm();
            tmp.fill_instance(instance);
            return ok(views.html.admin.edit_money_account.render( acForm.fill(tmp) ));
        }
        return ok("NO SUCH ACCOUNT");
    }
}
