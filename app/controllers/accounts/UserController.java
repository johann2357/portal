package controllers;

import models.UserAccount;
import play.*;
import play.mvc.*;
import java.util.Date;
import views.html.*;
import models.*;
import play.data.Form;

public class UserController extends Controller {

    static Form<UserAccount> userForm = Form.form(UserAccount.class);

    public static Result index() {
        return ok(views.html.accounts.new_user.render(userForm));
    }

    public static Result add_user(){
        Form<UserAccount> uForm = userForm.bindFromRequest();

        if(uForm.hasErrors()) {
            return badRequest(
                    views.html.index.render("Error en registro")
            );
        } else {
            UserAccount.add(uForm.get());
            return redirect(routes.Application.index());
        }
    }
}


