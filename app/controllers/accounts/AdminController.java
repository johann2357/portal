package controllers;
import play.*;
import play.mvc.*;
import java.util.Date;
import java.util.List;
import java.util.Arrays;
import java.util.Map;
import java.util.HashMap;
import play.data.Form;
import models.*;
import models.UserAccount;
import forms.AdminAccountForm;

public class AdminController extends Controller {

    static Form<AdminAccountForm> admin_ac_form = Form.form(AdminAccountForm.class);
    static Form<UserAccount> userForm = Form.form(UserAccount.class);

    public static Result index() {
        if (! LoginController.is_admin()) {
            return redirect(routes.LoginAdminController.login());
        }
        String user = session("email");
        if(user != null) {
            Map<String, String> admin_options = new HashMap<String, String>();
            admin_options.put("file", "File");
            admin_options.put("category", "Category");
            admin_options.put("perm_promotion", "Permanent Promotion");
            admin_options.put("temp_promotion", "Temporary Promotion");
            admin_options.put("moneyaccount", "User's Money Account");
            admin_options.put("newadmin", "Create New Admin Account");
            return ok(views.html.admin.index.render(admin_options,session().get("email")));
        } else {
            return ok(views.html.error.render("/login","Oops, you are not connected"));
        }
    }
    public static Result add_admin() {
        if (! LoginController.is_admin()) {
            return redirect(routes.LoginAdminController.login());
        }
        return ok(views.html.admin.add_admin.render( admin_ac_form ));
    }
    public static Result save_admin() {
        if (! LoginController.is_admin()) {
            return redirect(routes.LoginAdminController.login());
        }
        Form<AdminAccountForm> filledForm = admin_ac_form.bindFromRequest();
        if(filledForm.hasErrors()) {
            return badRequest(views.html.admin.add_admin.render(filledForm));
        } else {
            AdminAccount saved_ac = new AdminAccount();
            AdminAccountForm ff = filledForm.get();
            saved_ac.nickname = ff.nickname;
            saved_ac.password = ff.password;
            saved_ac.email = ff.email;
            saved_ac.save();
            return redirect(routes.AdminController.index());
        }
    }

    public static Result open1(){
        return TODO;
        //return ok(views.html.admin.consulta1.render(userForm,new UserAccount()));
    }
    public static Result consulta_1(){
        return TODO;
        /*
        Form<UserAccount> userAcc = userForm.bindFromRequest();
        UserAccount tmp = userAcc.get();
        System.out.println(tmp.id);
        if (userAcc.hasErrors()){
            //return badRequest(views.html.admin.consulta1.render(userAcc,userAcc.get()));
        }else{
            UserAccount.getAccount(userAcc.get());
            //return ok(views.html.admin.consulta1.render(userAcc,userAcc.get()));
        }
        */
    }
}
