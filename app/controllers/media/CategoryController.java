package controllers;
import play.*;
import play.mvc.*;
import play.data.Form;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import views.html.*;
import models.*;
import forms.CategoryForm;

public class CategoryController extends Controller {

    static Form<CategoryForm> ctgForm = Form.form(CategoryForm.class);

    public static Result list() {
        if (! LoginController.is_admin()) {
            return redirect(routes.LoginAdminController.login());
        }
        return ok(views.html.admin.list_categories.render( Category.find.all() ));
    }

    public static Result add() {
        if (! LoginController.is_admin()) {
            return redirect(routes.LoginAdminController.login());
        }
        return ok(views.html.admin.add_category.render( ctgForm, Category.find.all() ));
    }

    public static Result save() {
        if (! LoginController.is_admin()) {
            return redirect(routes.LoginAdminController.login());
        }
        Form<CategoryForm> filledForm = ctgForm.bindFromRequest();
        if(filledForm.hasErrors()) {
            return badRequest(views.html.admin.add_category.render(filledForm, Category.find.all()));
        } else {
            Category saved_ctg = new Category();
            CategoryForm ff = filledForm.get();
            saved_ctg.name = ff.name;
            saved_ctg.description = ff.description;
            if (ff.id_parent != null) {
                saved_ctg.parent = Category.find.byId(ff.id_parent);
            }
            saved_ctg.save();
            return redirect(routes.CategoryController.list());
        }
    }

    public static Result update() {
        if (! LoginController.is_admin()) {
            return redirect(routes.LoginAdminController.login());
        }
        Form<CategoryForm> filledForm = ctgForm.bindFromRequest();
        if(filledForm.hasErrors()) {
            return badRequest(views.html.admin.edit_category.render(filledForm, Category.find.all()));
        } else {
            CategoryForm ff = filledForm.get();
            Category saved_ctg = Category.find.byId(ff.id);
            saved_ctg.id = ff.id;
            saved_ctg.name = ff.name;
            saved_ctg.description = ff.description;
            if (ff.id_parent != null) {
                saved_ctg.parent = Category.find.byId(ff.id_parent);
            }
            saved_ctg.update();
            return redirect(routes.CategoryController.list());
        }
    }

    public static Result edit(Long id) {
        if (! LoginController.is_admin()) {
            return redirect(routes.LoginAdminController.login());
        }
        Category instance = Category.find.byId(id);
        if (instance != null) {
            CategoryForm tmp = new CategoryForm();
            tmp.fill_instance(instance);
            return ok(views.html.admin.edit_category.render( ctgForm.fill(tmp), Category.find.all() ));
        }
        return ok("NO SUCH CATEGORY");
    }
}
