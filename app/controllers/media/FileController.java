package controllers;
import play.*;
import play.mvc.*;
import play.data.Form;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import views.html.*;
import models.*;
import forms.FileForm;

public class FileController extends Controller {

    static Form<FileForm> fileForm = Form.form(FileForm.class);

    public static Result list() {
        if (! LoginController.is_admin()) {
            return redirect(routes.LoginAdminController.login());
        }
        return ok(views.html.admin.list_files.render(models.File.all_File()));
    }

    public static Result add() {
        if (! LoginController.is_admin()) {
            return redirect(routes.LoginAdminController.login());
        }
        return ok(views.html.admin.add_file.render(fileForm, Category.find.all()));
    }

    public static Result save() {
        if (! LoginController.is_admin()) {
            return redirect(routes.LoginAdminController.login());
        }
        // Para que funcione la subida de archivos ... se tiene
        // que crear una carpeta vacia dentro de 'public' llamada 'media'
        Form<FileForm> filledForm = fileForm.bindFromRequest();
        File saved_file = new File();
        if(filledForm.hasErrors()) {
            return badRequest(views.html.admin.add_file.render(filledForm, Category.find.all()));
        } else {
            FileForm ff = filledForm.get();
            saved_file.name = ff.name;
            saved_file.author = ff.author;
            saved_file.description = ff.description;
            saved_file.price = ff.price;
            saved_file.rate = ff.rate;
            saved_file.state = ff.state;
            if (ff.category != null) {
                saved_file.category = Category.find.byId(ff.category);
            }
            saved_file.size = 0L;
            saved_file.path = "-";
            saved_file.save();
        }
        //
        Http.MultipartFormData body = request().body().asMultipartFormData();
        Http.MultipartFormData.FilePart mediafile = body.getFile("mediafile");
        if (mediafile != null) {
            String fileName = mediafile.getFilename();
            String contentType = mediafile.getContentType(); 
            // TODO : restringir archivos por contentType
            System.out.println(contentType);
            java.io.File file = mediafile.getFile();
            saved_file.size = file.length();
            // TODO : agregar la extension del archivo de acuerdo al contentType
            saved_file.path = "media/" + saved_file.id;
            saved_file.update();
            file.renameTo(new java.io.File(saved_file.path));
            return redirect(routes.FileController.list());
        } else {
            flash("error", "Missing file");
            saved_file.delete();
            return ok("File was not uploaded");
        }
    }

    public static Result update() {
        if (! LoginController.is_admin()) {
            return redirect(routes.LoginAdminController.login());
        }
        Form<FileForm> filledForm = fileForm.bindFromRequest();
        File saved_file;
        if(filledForm.hasErrors()) {
            return badRequest(views.html.admin.edit_file.render(filledForm, Category.find.all()));
        } else {
            FileForm ff = filledForm.get();
            saved_file = File.find.byId(ff.id);
            saved_file.name = ff.name;
            saved_file.author = ff.author;
            saved_file.description = ff.description;
            saved_file.price = ff.price;
            saved_file.rate = ff.rate;
            saved_file.state = ff.state;
            if (ff.category != null) {
                saved_file.category = Category.find.byId(ff.category);
            }
            saved_file.size = ff.size;
            saved_file.update();
        }
        //
        Http.MultipartFormData body = request().body().asMultipartFormData();
        Http.MultipartFormData.FilePart mediafile = body.getFile("mediafile");
        if (mediafile != null) {
            String fileName = mediafile.getFilename();
            String contentType = mediafile.getContentType(); 
            // TODO : restringir archivos por contentType
            System.out.println(contentType);
            java.io.File file = mediafile.getFile();
            saved_file.size = file.length();
            // TODO : agregar la extension del archivo de acuerdo al contentType
            saved_file.path = "media/" + saved_file.id;
            saved_file.update();
            file.renameTo(new java.io.File(saved_file.path));
        }
        return redirect(routes.FileController.list());
    }

    public static Result edit(Long id) {
        if (! LoginController.is_admin()) {
            return redirect(routes.LoginAdminController.login());
        }
        File instance = File.find.byId(id);
        if (instance != null) {
            FileForm tmp = new FileForm();
            tmp.fill_instance(instance);
            return ok(views.html.admin.edit_file.render( fileForm.fill(tmp), Category.find.all() ));
        }
        return ok("NO SUCH FILE");
    }

    public static Result user_list() {
        if (! LoginController.is_user()) {
            return redirect(routes.LoginController.login());
        }
        return ok(views.html.media.list_files.render(
            File.find.where().eq("state", "OK").findList()
        ));
    }
}
