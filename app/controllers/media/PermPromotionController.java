package controllers;
import play.*;
import play.mvc.*;
import play.data.Form;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import views.html.*;
import models.*;
import forms.PermanentPromForm;

public class PermPromotionController extends Controller {

	static Form<PermanentPromForm> permanentForm = Form.form(PermanentPromForm.class);
	
	public static Result list() {
        if (! LoginController.is_admin()) {
            return redirect(routes.LoginAdminController.login());
        }
		return ok(views.html.admin.list_permanent_prom.render(models.PermanentPromotion.find.all()));
	}

	public static Result add() {
        if (! LoginController.is_admin()) {
            return redirect(routes.LoginAdminController.login());
        }
		return ok(views.html.admin.add_permanent_prom.render(permanentForm, Category.find.all()));
	}

	public static Result save() {
        if (! LoginController.is_admin()) {
            return redirect(routes.LoginAdminController.login());
        }
		Form<PermanentPromForm> ppForm = permanentForm.bindFromRequest();
		PermanentPromotion saved_pp = new PermanentPromotion();
		if(ppForm.hasErrors()) {
			return badRequest(views.html.admin.add_permanent_prom.render(ppForm, Category.find.all()));
		} 
		else {
			PermanentPromForm ppf = ppForm.get();
			saved_pp.name = ppf.name;
			saved_pp.discount = ppf.discount;
			Date d = saved_pp.StringToDate(ppf.begin_date);
			saved_pp.begin_date = d; //ppf.begin_date;
			saved_pp.description = ppf.description;
			//saved_pp.category = ppf.category;
			if (ppf.category != null) {
				saved_pp.category = Category.find.byId(ppf.category);
			}

			saved_pp.money_required = ppf.money_required;
			saved_pp.save();
		}
		return ok(views.html.admin.list_permanent_prom.render(models.PermanentPromotion.find.all()));
	}

	public static Result update() {
        if (! LoginController.is_admin()) {
            return redirect(routes.LoginAdminController.login());
        }
		Form<PermanentPromForm> ppForm = permanentForm.bindFromRequest();
		PermanentPromotion saved_pp;
		if(ppForm.hasErrors()) {
			return badRequest(views.html.admin.edit_permanent_prom.render(ppForm, Category.find.all()));
		} else {
			PermanentPromForm ppf = ppForm.get();
			saved_pp = PermanentPromotion.find.byId(ppf.id);
			saved_pp.name = ppf.name;
			saved_pp.discount = ppf.discount;
			Date d = saved_pp.StringToDate(ppf.begin_date);
			saved_pp.begin_date = d; //ppf.begin_date;
			saved_pp.description = ppf.description;
			//saved_pp.category = ppf.category;
			if (ppf.category != null) {
				saved_pp.category = Category.find.byId(ppf.category);
			}
			saved_pp.money_required = ppf.money_required;
			saved_pp.update();
		}
		return ok(views.html.admin.list_permanent_prom.render(models.PermanentPromotion.find.all()));
		//return redirect(routes.PermPromotionController.list());

	}

	public static Result edit(Long id) {
        if (! LoginController.is_admin()) {
            return redirect(routes.LoginAdminController.login());
        }
		PermanentPromotion instance = PermanentPromotion.find.byId(id);
		if (instance != null) {
			PermanentPromForm tmp = new PermanentPromForm();
			tmp.fill_instance(instance);
			return ok(views.html.admin.edit_permanent_prom.render( permanentForm.fill(tmp), Category.find.all()));
		}
		return ok("NO SUCH CATEGORY");
	}

	public static Result delete(Long id) {
        if (! LoginController.is_admin()) {
            return redirect(routes.LoginAdminController.login());
        }
		PermanentPromotion instance = PermanentPromotion.find.byId(id);
		instance.delete();
		return ok(views.html.admin.list_permanent_prom.render(models.PermanentPromotion.find.all()));
	}
}
