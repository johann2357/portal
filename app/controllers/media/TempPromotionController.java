package controllers;
import play.*;
import play.mvc.*;
import play.data.Form;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import views.html.*;
import models.*;
import forms.TemporaryPromForm;

public class TempPromotionController extends Controller {

	static Form<TemporaryPromForm> temporaryForm = Form.form(TemporaryPromForm.class);

	public static Result list() {
        if (! LoginController.is_admin()) {
            return redirect(routes.LoginAdminController.login());
        }
		return ok(views.html.admin.list_temporary_prom.render(models.TemporaryPromotion.get_all()));
	}

	public static Result add() {
        if (! LoginController.is_admin()) {
            return redirect(routes.LoginAdminController.login());
        }
	   return ok(views.html.admin.add_temporary_prom.render(temporaryForm, Category.find.all()));
	}

	public static Result save() {
        if (! LoginController.is_admin()) {
            return redirect(routes.LoginAdminController.login());
        }
		Form<TemporaryPromForm> tpForm = temporaryForm.bindFromRequest();
		TemporaryPromotion saved_tp = new TemporaryPromotion();
		if(tpForm.hasErrors()) {
			return badRequest(views.html.admin.add_temporary_prom.render(tpForm, Category.find.all()));
		} 
		else {
			TemporaryPromForm tpf = tpForm.get();
			saved_tp.name = tpf.name;
			saved_tp.discount = tpf.discount;
			Date d1 = saved_tp.StringToDate(tpf.begin_date);
			saved_tp.begin_date = d1; //tpf.begin_date;
			saved_tp.description = tpf.description;
			//saved_tp.category = tpf.category;
			if (tpf.category != null) {
				saved_tp.category = Category.find.byId(tpf.category);
			}
			Date d2 = saved_tp.StringToDate(tpf.end_date);
			saved_tp.end_date = d2;
			saved_tp.save();
		}
		return ok(views.html.admin.list_temporary_prom.render(models.TemporaryPromotion.find.all()));
	}

	public static Result update() {
        if (! LoginController.is_admin()) {
            return redirect(routes.LoginAdminController.login());
        }
		Form<TemporaryPromForm> tpForm = temporaryForm.bindFromRequest();
		TemporaryPromotion saved_tp;
		if(tpForm.hasErrors()) {
			return badRequest(views.html.admin.edit_temporary_prom.render(tpForm, Category.find.all()));
		} else {
			TemporaryPromForm tpf = tpForm.get();
			saved_tp = TemporaryPromotion.find.byId(tpf.id);
			saved_tp.name = tpf.name;
			saved_tp.discount = tpf.discount;
			Date d1 = saved_tp.StringToDate(tpf.begin_date);
			saved_tp.begin_date = d1;
			saved_tp.description = tpf.description;
			if (tpf.category != null) {
				saved_tp.category = Category.find.byId(tpf.category);
			}
			Date d2 = saved_tp.StringToDate(tpf.end_date);
			saved_tp.end_date = d2;
			saved_tp.update();
		}
        return redirect(routes.TempPromotionController.list());
	}

	public static Result edit(Long id) {
        if (! LoginController.is_admin()) {
            return redirect(routes.LoginAdminController.login());
        }
		TemporaryPromotion instance = TemporaryPromotion.find.byId(id);
		if (instance != null) {
			TemporaryPromForm tmp = new TemporaryPromForm();
			tmp.fill_instance(instance);
			return ok(views.html.admin.edit_temporary_prom.render( temporaryForm.fill(tmp), Category.find.all()));
		}
		return ok("NO SUCH CATEGORY");
	}

	public static Result delete(Long id) {
        if (! LoginController.is_admin()) {
            return redirect(routes.LoginAdminController.login());
        }
		TemporaryPromotion instance = TemporaryPromotion.find.byId(id);
		instance.delete();
		return ok(views.html.admin.list_temporary_prom.render(models.TemporaryPromotion.get_all()));
		//return TODO;
	}
}
