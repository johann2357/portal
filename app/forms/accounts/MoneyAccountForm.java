package forms;

import java.util.*;
import play.data.validation.Constraints.*;
import play.data.validation.ValidationError;
import models.MoneyAccount;


public class MoneyAccountForm {

    public Long id;
    public float amount;

    public List<ValidationError> validate() { 
        List<ValidationError> errors = new ArrayList<ValidationError>(); 
        if (this.amount < 0) {
            errors.add(new ValidationError("amount",  "Must be positive!"));
        }
        return errors.isEmpty() ? null : errors;
    }

    public void fill_instance(MoneyAccount ac) 
    {
        this.id = ac.id;
        this.amount = ac.amount;
    }

}