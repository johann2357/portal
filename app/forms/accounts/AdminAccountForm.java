package forms;

import java.util.*;
import play.data.validation.Constraints.*;
import play.data.validation.ValidationError;
import models.AdminAccount;
import models.UserAccount;


public class AdminAccountForm {

    @Required
    public String nickname;
    @Email
    @Required
    public String email;
    @Required
    public String password;

    public List<ValidationError> validate() { 
        List<ValidationError> errors = new ArrayList<ValidationError>(); 
        AdminAccount tmp = AdminAccount.find.where().eq("nickname", this.nickname).findUnique();
        UserAccount tmp2 = UserAccount.find.where().eq("nickname", this.nickname).findUnique();
        if ((tmp != null) || (tmp2 != null)) {
            errors.add(new ValidationError("nickname",  "That nickname already exists"));
        }
        tmp = AdminAccount.find.where().eq("email", this.email).findUnique();
        tmp2 = UserAccount.find.where().eq("email", this.email).findUnique();
        if ((tmp != null) || (tmp2 != null)) {
            errors.add(new ValidationError("email",  "That email already exists"));
        }
        return errors.isEmpty() ? null : errors;
    }
}