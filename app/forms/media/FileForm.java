package forms;

import java.util.*;
import play.data.validation.Constraints.*;
import play.data.validation.ValidationError;
import models.File;

public class FileForm {
    
    public Long id;
    public Long size;
    public String name;
    public String author;
    public String description;
    public String path;
    @Required
    public float price;
    @Required
    public int rate;
    @Required
    public String state;
    public Long category;

    public List<ValidationError> validate() { 
        // null indica que esta bien ...  caso contrario retorna el string como mensaje
        List<ValidationError> errors = new ArrayList<ValidationError>(); 
        // validando price
        if (this.price <= 0) {
            errors.add(new ValidationError("price",  "Price can't be a negative number nor 0"));
        }
        // validando rate
        if ((this.rate < 0) || (this.rate > 10)) {
            errors.add(new ValidationError("rate",  "Initial rate must be between 0 and 10"));
        }
        // validando state
        if (!((this.state.equals("OK")) || (this.state.equals("NN")))) {
            errors.add(new ValidationError("state",  "Option is not valid. Valid options ('OK', 'NN')"));
        }
        return errors.isEmpty() ? null : errors;
    }

    public void fill_instance(File f) {
        if (f.category != null)
            this.category = f.category.id;
        this.id = f.id;
        this.size = f.size;
        this.name = f.name;
        this.author = f.author;
        this.description = f.description;
        this.price = f.price;
        this.rate = f.rate;
        this.state = f.state;
        this.path = f.path;
    }
}
