package forms;

import java.util.*;
import play.data.validation.Constraints.*;
import play.data.validation.ValidationError;
import models.Category;
import models.PermanentPromotion;

public class PermanentPromForm {
    
    public Long id;
    public String name;
    @Required
    public float discount;
    @Required
    public String begin_date;
    public String description;
    @Required
    public Long category;
    //public models.Category category;
    @Required
    public float money_required;


    public List<ValidationError> validate() { 
        List<ValidationError> errors = new ArrayList<ValidationError>(); 
        // validando discount
        if (this.discount <= 0 ) {
            errors.add(new ValidationError("discount",  "Discount can't be a negative number or 0"));
        }
        // validando money_required
        if (this.money_required <= 0) {
            errors.add(new ValidationError("money_required",  "Money Required can't be a negative number or 0"));
        }
        return errors.isEmpty() ? null : errors;
    }

    public void fill_instance(PermanentPromotion pp) {
        if (pp.category != null)
            this.category =pp.category.id;
        this.id = pp.id;
        this.name = pp.name;
        this.discount = pp.discount;
        this.begin_date = pp.getFecha(pp.begin_date);
        this.description = pp.description;
        this.money_required = pp.money_required;
    }
}
