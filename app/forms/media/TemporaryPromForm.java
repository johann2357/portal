package forms;

import java.util.*;
import play.data.validation.Constraints.*;
import play.data.validation.ValidationError;
import models.Category;
import models.TemporaryPromotion;

public class TemporaryPromForm {
    
    public Long id;
    public String name;
    @Required
    public float discount;
    @Required
    public String begin_date;
    public String description;
    @Required
    public Long category;
    //public models.Category category;
    @Required
    public String end_date;


    public List<ValidationError> validate() { 
        List<ValidationError> errors = new ArrayList<ValidationError>(); 
        // validando discount
        if (this.discount <= 0 ) {
            errors.add(new ValidationError("discount",  "Discount can't be a negative number or 0"));
        }
        // validando money_required
        /*if (this.end_date.before(begin_date)) {
            errors.add(new ValidationError("end_date",  "End_date is less than the begin_date"));
        }*/
        return errors.isEmpty() ? null : errors;
    }

    public void fill_instance(TemporaryPromotion tp) {
        if (tp.category != null)
            this.category =tp.category.id;
        this.id = tp.id;
        this.name = tp.name;
        this.discount = tp.discount;
        this.begin_date = tp.getFecha(tp.begin_date);
        this.description = tp.description;
        this.end_date = tp.getFecha(tp.end_date);
    }
}
