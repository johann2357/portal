package forms;

import java.util.*;
import play.data.validation.Constraints.*;
import play.data.validation.ValidationError;
import models.Category;


public class CategoryForm {

    public Long id;
    
    @Required
    public String name;
    
    public String description;

    public Long id_parent;

    public List<ValidationError> validate() { 
        List<ValidationError> errors = new ArrayList<ValidationError>(); 
        // validando name
        if (this.name == "") {
            errors.add(new ValidationError("name",  "Must be a name for this category"));
        }
        Category tmp = Category.find.where().eq("name", this.name).findUnique();
        if ((tmp != null) && (tmp.id != this.id)) {
            errors.add(new ValidationError("name",  "Must be a unique name for this category"));
        }
        return errors.isEmpty() ? null : errors;
    }

    public void fill_instance(Category cat) 
    {
        this.id = cat.id;
        this.name = cat.name;
        if (cat.parent != null)
            this.id_parent = cat.parent.id;
        this.description  = cat.description;
    }
}