package forms;

import java.util.*;
import play.data.validation.Constraints.*;
import play.data.validation.ValidationError;
import models.Purchase;

public class PurchaseForm {
    
    public Long id;
    @Required
    public int rate;
    public Long file;

    public List<ValidationError> validate() { 
        List<ValidationError> errors = new ArrayList<ValidationError>(); 
        // validando rate
        if ((this.rate > 1) || (this.rate < -1)) {
            errors.add(new ValidationError("rate",  "Rate must be 0, 1 or -1"));
        }
        return errors.isEmpty() ? null : errors;
    }

    public void fill_instance(Purchase f) {
        this.id = f.id;
        this.rate = f.rate;
        this.file = f.related_file.id;
    }
}
